package com.example.tiefighters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.util.*

public class TIEFighter(var context: Context) {
    var tieFighter: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.tiesmall)
    public var ex: Int
    var ey: Int
    public var enemyVelocity: Int
    var random: Random

    init {
        random = Random()
        ex = 200 + random.nextInt(400)
        ey = 0
        enemyVelocity = 14 + random.nextInt(10)
    }

    val enemySpaceshipWidth: Int
        get() = tieFighter.width ?: 0
    val enemySpaceshipHeight: Int
        get() = tieFighter.height ?: 0
}