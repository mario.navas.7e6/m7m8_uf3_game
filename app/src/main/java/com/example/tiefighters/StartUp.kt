package com.example.tiefighters

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tiefighters.R
import android.content.Intent
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.tiefighters.MainActivity

class StartUp : AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.startup)
    }

    fun startGame(view: View?) {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    fun help(view: View?){
        var helpView = findViewById<ConstraintLayout>(R.id.help)
        helpView.visibility =View.VISIBLE
    }

    fun close(view: View?){
        var helpView = findViewById<ConstraintLayout>(R.id.help)
        helpView.visibility =View.INVISIBLE

    }
}