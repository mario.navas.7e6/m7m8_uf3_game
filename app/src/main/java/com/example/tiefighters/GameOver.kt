package com.example.tiefighters

import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import android.os.Bundle
import com.example.tiefighters.R
import android.content.Intent
import android.view.View
import com.example.tiefighters.StartUp

class GameOver : AppCompatActivity() {
    private var tvPoints: TextView? = null
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_over_test)
        val points = intent.extras!!.getInt("points")
        tvPoints = findViewById(R.id.tvPoints)
        tvPoints!!.text = points.toString()
    }

    fun restart(view: View?) {
        val intent = Intent(this@GameOver, StartUp::class.java)
        startActivity(intent)
        finish()
    }

    fun exit(view: View?) {
        finish()
    }
}