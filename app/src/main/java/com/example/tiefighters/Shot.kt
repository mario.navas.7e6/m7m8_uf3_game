package com.example.tiefighters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.tiefighters.R

class Shot(var context: Context, shx: Int, shy: Int, type: Boolean) {
    var shot: Bitmap? = null
    var shx: Int
    var shy: Int

    init {
        shot = if (type) {
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.minishotgreen
            )
        } else BitmapFactory.decodeResource(
            context.resources,
            R.drawable.shot
        )
        this.shx = shx
        this.shy = shy
    }

    val shotWidth: Int
        get() = shot!!.width?:0
    val shotHeight: Int
        get() = shot!!.height?:0
}