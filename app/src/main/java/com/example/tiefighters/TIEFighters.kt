package com.example.tiefighters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import java.util.*

internal class TIEFighters(context: Context) : View(
    context
) {
    var background: Bitmap?
    var lifeImage: Bitmap
    var handlerer: Handler? = null
    var UPDATE_MILLIS: Long = 30
    var points = 0
    var life = 3
    var scorePaint: Paint
    var TEXT_SIZE = 80
    var paused = false
    var milleniumFalcoon: MilleniumFalcoon
    var tieFighter: TIEFighter
    var random: Random
    var enemyShots: ArrayList<Shot>
    var ourShots: ArrayList<Shot>
    var explosion: Explosion? = null
    var explosions: ArrayList<Explosion>
    var enemyShotAction = false
    val runnable = Runnable { invalidate() }

    init {
        val display = (getContext() as Activity).windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        screenWidth = size.x
        screenHeight = size.y
        random = Random()
        enemyShots = ArrayList()
        ourShots = ArrayList()
        explosions = ArrayList()
        milleniumFalcoon = MilleniumFalcoon(context)
        tieFighter = TIEFighter(context)
        handlerer = Handler()
        background = BitmapFactory.decodeResource(context.resources, R.drawable.background2)
        lifeImage = BitmapFactory.decodeResource(context.resources, R.drawable.life)
        scorePaint = Paint()
        scorePaint.color = Color.RED
        scorePaint.textSize = TEXT_SIZE.toFloat()
        scorePaint.textAlign = Paint.Align.LEFT
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        background?.let { canvas.drawBitmap(it, 0f, 0f, null) }
        canvas.drawText("Pt: $points", 0f, TEXT_SIZE.toFloat(), scorePaint)
        for (i in life downTo 1) {
            canvas.drawBitmap(lifeImage, (screenWidth - lifeImage.width * i).toFloat(), 0f, null)
        }
        if (life == 0) {
            paused = true
            handlerer = null
            val intent = Intent(context, GameOver::class.java)
            intent.putExtra("points", points)
            context.startActivity(intent)
            (context as Activity).finish()
        }
        tieFighter.ex += tieFighter.enemyVelocity
        if (tieFighter.ex + tieFighter.enemySpaceshipWidth >= screenWidth) {
            tieFighter.enemyVelocity *= -1
        }
        if (tieFighter.ex <= 0) {
            tieFighter.enemyVelocity *= -1
        }


        if (!enemyShotAction) {
            if (tieFighter.ex >= 200 + random.nextInt(400)) {
                val enemyShot = Shot(
                    context,
                    tieFighter.ex + tieFighter.enemySpaceshipWidth / 2,
                    tieFighter.ey,
                    false
                )
                enemyShots.add(enemyShot)
                enemyShotAction = true
            }
            enemyShotAction = if (tieFighter.ex >= 400 + random.nextInt(800)) {
                val enemyShot = Shot(
                    context,
                    tieFighter.ex + tieFighter.enemySpaceshipWidth / 2,
                    tieFighter.ey,
                    false
                )
                enemyShots.add(enemyShot)
                true
            } else {
                val enemyShot = Shot(
                    context,
                    tieFighter.ex + tieFighter.enemySpaceshipWidth / 2,
                    tieFighter.ey,
                    false
                )
                enemyShots.add(enemyShot)
                true
            }
        }


        canvas.drawBitmap(
            tieFighter.tieFighter,
            tieFighter.ex.toFloat(),
            tieFighter.ey.toFloat(),
            null
        )
        if (milleniumFalcoon.ox > screenWidth - milleniumFalcoon.ourSpaceshipWidth) {
            milleniumFalcoon.ox = screenWidth - milleniumFalcoon.ourSpaceshipWidth
        } else if (milleniumFalcoon.ox < 0) {
            milleniumFalcoon.ox = 0
        }
        canvas.drawBitmap(
            milleniumFalcoon.milleniumFalcoon,
            milleniumFalcoon.ox.toFloat(),
            milleniumFalcoon.oy.toFloat(),
            null
        )

        for (i in enemyShots.indices) {
            if (i >= enemyShots.size) {
                break
            }

            enemyShots[i].shy += 15
            enemyShots[i].shot?.let {
                canvas.drawBitmap(
                        it,
                        enemyShots[i].shx.toFloat(),
                        enemyShots[i].shy.toFloat(),
                        null
                )
            }

            if (enemyShots[i].shx >= milleniumFalcoon.ox
                    && enemyShots[i].shx <= milleniumFalcoon.ox + milleniumFalcoon.ourSpaceshipWidth && enemyShots[i].shy >= milleniumFalcoon.oy && enemyShots[i].shy <= screenHeight
            ) {
                life--
                enemyShots.removeAt(i)
                explosion = Explosion(context, milleniumFalcoon.ox, milleniumFalcoon.oy)
                explosions.add(explosion!!)
            } else if (enemyShots[i].shy >= screenHeight) {
                enemyShots.removeAt(i)
            }

            if (enemyShots.size < 1) {
                enemyShotAction = false
            }
        }

        for (i in ourShots.indices) {
            ourShots[i].shy -= 15
            ourShots[i].shot?.let {
                canvas.drawBitmap(
                    it,
                    ourShots[i].shx.toFloat(),
                    ourShots[i].shy.toFloat(),
                    null
                )
            }
            if (ourShots[i].shx >= tieFighter.ex
                && ourShots[i].shx <= tieFighter.ex + tieFighter.enemySpaceshipWidth && ourShots[i].shy <= tieFighter.enemySpaceshipWidth && ourShots[i].shy >= tieFighter.ey
            ) {
                points++
                ourShots.removeAt(i)
                explosion = Explosion(context, tieFighter.ex, tieFighter.ey)
                explosions.add(explosion!!)
            } else if (ourShots[i].shy <= 0) {
                ourShots.removeAt(i)
            }
        }
//        for (i in explosions.indices) {
//            explosions[i].getExplosion(explosions[i].explosionFrame)?.let {
//                canvas.drawBitmap(
//                    it,
//                    explosions[i].eX.toFloat(),
//                    explosions[i].eY.toFloat(),
//                    null
//                )
//            }
//            explosions[i].explosionFrame++
//            if (explosions[i].explosionFrame > 8) {
//                explosions.removeAt(i)
//            }
//        }

        if (!paused) handlerer!!.postDelayed(runnable, UPDATE_MILLIS)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val touchX = event.x.toInt()

        if (event.action == MotionEvent.ACTION_UP) {
            if (ourShots.size < 1) {
                val ourShot = Shot(
                    context,
                    milleniumFalcoon.ox + milleniumFalcoon.ourSpaceshipWidth / 2,
                    milleniumFalcoon.oy,
                    true
                )
                ourShots.add(ourShot)
            }
        }
        if (event.action == MotionEvent.ACTION_DOWN) {
            milleniumFalcoon.ox = touchX
        }

        if (event.action == MotionEvent.ACTION_MOVE) {
            milleniumFalcoon.ox = touchX
        }

        return true
    }

    companion object {
        @JvmField
        var screenWidth: Int = 0

        @JvmField
        var screenHeight: Int = 0
    }
}