package com.example.tiefighters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.util.*

class MilleniumFalcoon(var context: Context) {
    var milleniumFalcoon: Bitmap
    var ox: Int
    var oy: Int
    var random: Random

    init {
        milleniumFalcoon =
            BitmapFactory.decodeResource(context.resources, R.drawable.millenniumfalconsmall)
        random = Random()
        ox = random.nextInt(TIEFighters.screenWidth)
        oy = TIEFighters.screenHeight - (milleniumFalcoon.height?: 0)
    }

    val ourSpaceshipWidth: Int
        get() = milleniumFalcoon.width?: 0
}